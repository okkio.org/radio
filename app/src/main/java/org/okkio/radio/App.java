package org.okkio.radio;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

import org.okkio.radio.event.BaseEvent;

public class App extends Application {
    public static final String KEY_QUEUE_POSITION = "queue_position";
    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_CATEGORY_TITLE = "category_title";
    private static App sInstance;
    private static Bus sBus;

    public static App getInstance() {
        if (sInstance == null) {
            sInstance = new App();
            sBus = new AppBus();
        }
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Bus getBus() {
        return sBus;
    }

    /**
     * Post any type of event from anywhere in the app
     *
     * @param event
     */
    public static void postToBus(BaseEvent event) {
        getInstance().getBus().post(event);
    }

    /**
     * Enable posting of events from either the main or background threads
     */
    public static class AppBus extends Bus {
        private final Handler mainThread = new Handler(Looper.getMainLooper());

        @Override
        public void post(final Object event) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                super.post(event);
            } else {
                mainThread.post(new Runnable() {
                    @Override
                    public void run() {
                        post(event);
                    }
                });
            }
        }
    }
}
