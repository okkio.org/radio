package org.okkio.radio.fragment;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.data.StationDataCache;
import org.okkio.radio.event.PlaybackServiceEvent;
import org.okkio.radio.event.QueuePositionEvent;
import org.okkio.radio.model.Station;
import org.okkio.radio.service.PlaybackService;
import org.okkio.radio.util.Helper;

import java.util.List;

public class RadioPlayerFragment extends BaseFragment implements View.OnClickListener, ServiceConnection {

    private static final String TAG = "RadioPlayerFragment";
    private static final String BUNDLE_STATE = "state";

    private TextView mStationTitle;
    private ImageButton mPlayStopBtn;
    private ImageButton mNextBtn;
    private ImageButton mPrevBtn;
    private ImageView mPlayerBackground;
    private ImageView mEqualizer;
    private AnimationDrawable mEqualizerAnimation;
    private ProgressBar mProgressBar;
    private MediaControllerCompat mMediaController;
    private List<Station> mQueue;
    private int mQueuePosition;
    private int mState;
    private View mView;
    private Application mAppContext;
    private String mName;
    private boolean mWasPlaying;
    private boolean mFirstTimeIn;


    public RadioPlayerFragment() {
    }

    public static RadioPlayerFragment newInstance(int queuePosition) {
        RadioPlayerFragment fragment = new RadioPlayerFragment();
        Bundle args = new Bundle();
        args.putInt(App.KEY_QUEUE_POSITION, queuePosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mQueuePosition = getArguments().getInt(App.KEY_QUEUE_POSITION);
        mQueue = StationDataCache.getStationDataCache().getStationList();

        mAppContext = (Application) getActivity().getApplicationContext();

        Intent intent = new Intent(mAppContext, PlaybackService.class);
        mAppContext.bindService(intent, this, 0);
        mAppContext.startService(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAppContext.unbindService(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.content_player, container, false);

        mStationTitle = mView.findViewById(R.id.item_title);
        setStationTitle();

        mPlayerBackground = mView.findViewById(R.id.player_background);
        mProgressBar = mView.findViewById(R.id.progress_bar);

        // create equalizer animation
        mEqualizer = mView.findViewById(R.id.equalizer);
        mEqualizer.setBackgroundResource(R.drawable.equalizer_anim);
        mEqualizer.setVisibility(View.INVISIBLE);
        mEqualizerAnimation = (AnimationDrawable) mEqualizer.getBackground();

        // setup player controls elements
        mPlayStopBtn = mView.findViewById(R.id.action_play_stop_button);
        mPlayStopBtn.setOnClickListener(this);
        mPrevBtn = mView.findViewById(R.id.action_prev_button);
        mPrevBtn.setOnClickListener(this);
        mNextBtn = mView.findViewById(R.id.action_next_button);
        mNextBtn.setOnClickListener(this);

        if (mQueuePosition == 0) {
            mPrevBtn.setVisibility(View.GONE);
        }
        if (mQueuePosition == mQueue.size() - 1) {
            mNextBtn.setVisibility(View.GONE);
        }

        if (savedInstanceState != null) {
            mFirstTimeIn = false;
            mState = savedInstanceState.getInt(BUNDLE_STATE);
            if (mState == PlaybackStateCompat.STATE_BUFFERING) {
                mPlayStopBtn.setImageResource(R.drawable.ic_stop_white_24dp);
                mPlayerBackground.setVisibility(View.INVISIBLE);
                mProgressBar.setVisibility(View.VISIBLE);
                mEqualizer.setVisibility(View.INVISIBLE);
            } else if (mState == PlaybackStateCompat.STATE_PLAYING) {
                mPlayStopBtn.setImageResource(R.drawable.ic_stop_white_24dp);
                mPlayerBackground.setVisibility(View.INVISIBLE);
                mEqualizer.setVisibility(View.VISIBLE);
                mEqualizerAnimation.start();
            } else {
                mPlayerBackground.setVisibility(View.VISIBLE);
                mEqualizer.setVisibility(View.INVISIBLE);
            }
        } else {
            mFirstTimeIn = true;
            mProgressBar.setVisibility(View.VISIBLE);
        }

        return mView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mState = mMediaController.getPlaybackState().getState();
        outState.putInt(BUNDLE_STATE, mState);
    }

    @Override
    public void onClick(View view) {
        mState = mMediaController.getPlaybackState().getState();
        switch (view.getId()) {

            case R.id.action_play_stop_button:
                // start playback
                if (mState == PlaybackStateCompat.STATE_NONE || mState == PlaybackStateCompat.STATE_STOPPED) {
                    playFromStationUri();
                    // stop playback
                } else if (mState == PlaybackStateCompat.STATE_BUFFERING
                        || mState == PlaybackStateCompat.STATE_PLAYING
                        || mState == PlaybackStateCompat.STATE_CONNECTING) {
                    mMediaController.getTransportControls().stop();
                }
                break;

            case R.id.action_prev_button:
                if (mState == PlaybackStateCompat.STATE_BUFFERING
                        || mState == PlaybackStateCompat.STATE_PLAYING
                        || mState == PlaybackStateCompat.STATE_CONNECTING) {
                    mMediaController.getTransportControls().stop();
                    mWasPlaying = true;
                }
                mMediaController.getTransportControls().skipToPrevious();
                if (mPlayerBackground.getVisibility() == View.VISIBLE)
                    Helper.fadeViewElement(mPlayerBackground, View.INVISIBLE, 1, 0);
                Helper.fadeViewElement(mProgressBar, View.VISIBLE, 0, 1);
                break;

            case R.id.action_next_button:
                if (mState == PlaybackStateCompat.STATE_BUFFERING
                        || mState == PlaybackStateCompat.STATE_PLAYING
                        || mState == PlaybackStateCompat.STATE_CONNECTING) {
                    mMediaController.getTransportControls().stop();
                    mWasPlaying = true;
                }
                mMediaController.getTransportControls().skipToNext();
                if (mPlayerBackground.getVisibility() == View.VISIBLE)
                    Helper.fadeViewElement(mPlayerBackground, View.INVISIBLE, 1, 0);
                Helper.fadeViewElement(mProgressBar, View.VISIBLE, 0, 1);
                break;
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service instanceof PlaybackService.ServiceBinder) {
            try {
                mMediaController = new MediaControllerCompat(mAppContext,
                        ((PlaybackService.ServiceBinder) service).getService().getMediaSessionToken());

                mMediaController.registerCallback(mMediaControllerCallback);
                mState = mMediaController.getPlaybackState().getState();

                // start playback as soon as player activity launches
                if (mState == PlaybackStateCompat.STATE_NONE || mState == PlaybackStateCompat.STATE_STOPPED) {
                    playFromStationUri();
                } else if (mState == PlaybackStateCompat.STATE_BUFFERING || mState == PlaybackStateCompat.STATE_PLAYING) {
                    // if already playing, stop and start the new selected stn
                    mMediaController.getTransportControls().stop();
                    playFromStationUri();
                }

            } catch (RemoteException e) {
                Log.e(TAG, "Error instantiating the media controller: %s" + e.getMessage());
            }
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.i(TAG, "Service unexpectedly disconnected");
    }

    private MediaControllerCompat.Callback mMediaControllerCallback =
            new MediaControllerCompat.Callback() {

                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat state) {
                    switch (state.getState()) {
                        case PlaybackStateCompat.STATE_NONE:
                        case PlaybackStateCompat.STATE_STOPPED:
                            mPlayStopBtn.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                            if (!mWasPlaying) {
                                Helper.fadeViewElement(mPlayerBackground, View.VISIBLE, 0, 1);
                            } else {
                                mWasPlaying = false;
                            }
                            if (mEqualizer.getVisibility() == View.VISIBLE) {
                                Helper.fadeViewElement(mEqualizer, View.INVISIBLE, 1, 0);
                                mEqualizerAnimation.stop();
                            }
                            break;
                        case PlaybackStateCompat.STATE_BUFFERING:
                            mPlayStopBtn.setImageResource(R.drawable.ic_stop_white_24dp);
                            Helper.fadeViewElement(mPlayerBackground, View.INVISIBLE, 0, 0);
                            break;
                        case PlaybackStateCompat.STATE_PLAYING:

                            mPlayStopBtn.setImageResource(R.drawable.ic_stop_white_24dp);
                            if (mPlayerBackground.getVisibility() == View.VISIBLE)
                                Helper.fadeViewElement(mPlayerBackground, View.INVISIBLE, 1, 0);
                            mEqualizerAnimation.start();
                            Helper.fadeViewElement(mEqualizer, View.VISIBLE, 0, 1);
                            break;
                    }
                }
            };

    //region Helpers
    @Subscribe
    public void getMessageEvent(PlaybackServiceEvent event) {
        String message = event.getMessage();
        switch (message) {
            case PlaybackServiceEvent.ON_PLAYBACK_ERROR:
            case PlaybackServiceEvent.ON_PLAYBACK_COMPLETION:
            case PlaybackServiceEvent.ON_AUDIO_FOCUS_LOSS:
            case PlaybackServiceEvent.ON_BECOMING_NOISY:
            case PlaybackServiceEvent.ON_NO_STREAM_FOUND:
                mPlayStopBtn.setImageResource(R.drawable.ic_play_arrow_white_24dp);
            case PlaybackServiceEvent.ON_BUFFERING_COMPLETE:
                Helper.fadeViewElement(mProgressBar, View.GONE, 1, 0);
                displayMessage(message);
                break;
        }
    }

    @Subscribe
    public void getQueuePositionEvent(QueuePositionEvent event) {
        mQueuePosition = event.getQueuePosition();
        setStationTitle();
        if (mQueuePosition == 0) {
            mPrevBtn.setVisibility(View.GONE);
        } else if (mQueuePosition == mQueue.size() - 1) {
            mNextBtn.setVisibility(View.GONE);
        } else {
            mNextBtn.setVisibility(View.VISIBLE);
            mPrevBtn.setVisibility(View.VISIBLE);
        }
    }

    private void setStationTitle() {
        mName = mQueue.get(mQueuePosition).getName();
        if (mName != null)
            mStationTitle.setText(mName);
    }

    private void playFromStationUri() {
        Station stn = mQueue.get(mQueuePosition);
        if (stn != null) {
            String name = stn.getName() != null ? stn.getName() : "";
            String slug = stn.getSlug() != null ? stn.getSlug() : "";
            String country = stn.getCountry() != null ? stn.getCountry() : "";
            String imageUrl = stn.getImage().getUrl() != null ? stn.getImage().getUrl() : "";
            String thumbUrl = stn.getImage().getThumb().getUrl() != null ? stn.getImage().getThumb().getUrl() : "";
            String url = Helper.getStream(stn);

            if (url != null) {
                Uri uri = Uri.parse(url);
                Bundle extras = new Bundle();
                extras.putParcelable(PlaybackService.EXTRA_STATION_URI, uri);
                extras.putString(PlaybackService.EXTRA_STATION_NAME, name);
                extras.putString(PlaybackService.EXTRA_STATION_SLUG, slug);
                extras.putString(PlaybackService.EXTRA_STATION_COUNTRY, country);
                extras.putString(PlaybackService.EXTRA_STATION_IMAGE_URL, imageUrl);
                extras.putString(PlaybackService.EXTRA_STATION_THUMB_URL, thumbUrl);
                extras.putInt(PlaybackService.EXTRA_STATION_QUEUE_POSITION, mQueuePosition);

                // playFromUri() works on emulators api 16-19, not on api 21+
                //mMediaController.getTransportControls().playFromUri(uri, extras);
                mMediaController.getTransportControls().playFromSearch("", extras);

                // show the progress bar while buffering the audio stream
                Helper.fadeViewElement(mProgressBar, View.VISIBLE, 0, 1);

            } else {
                displayMessage(PlaybackServiceEvent.ON_NO_STREAM_FOUND);
            }
        }
    }

    private void displayMessage(String message) {
        Helper.showSnackbar(mView, message);
    }

    //endregion
}
