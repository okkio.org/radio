package org.okkio.radio.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import org.okkio.radio.App;
import org.okkio.radio.event.CategoryThreadCompletionEvent;
import org.okkio.radio.event.DataModelUpdateEvent;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.model.Category;
import org.okkio.radio.network.CategoryThread;
import org.okkio.radio.util.Helper;

import java.util.ArrayList;
import java.util.List;

public class CategoryDataFragment extends BaseFragment {
    public static final String CATEGORY_DATA_FRAGMENT_TAG = "category_data_fragment";
    private List<Category> mCategoryList = new ArrayList<>();
    private boolean mIsStarted = false;

    public CategoryDataFragment() {
    }

    public static CategoryDataFragment newInstance() {
        return new CategoryDataFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (getActivity() != null && Helper.isClientConnected(getActivity())) {
            if (!mIsStarted) {
                mIsStarted = true;
                new CategoryThread("CategoryThread", getActivity()).start();
            }
        } else {
            App.postToBus(new MessageEvent("Not connected, check connection"));
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    public ArrayList<Category> getCategoryData() {
        return new ArrayList<>(mCategoryList);
    }

    public Category getCategoryDataItem(int position) {
        return mCategoryList.get(position);
    }


    @Subscribe
    public void getCategoryList(CategoryThreadCompletionEvent event) {
        mIsStarted = false; // thread complete
        mCategoryList = event.getCategoryList();
        App.postToBus(new DataModelUpdateEvent(DataModelUpdateEvent.CATEGORY_MODEL_DATA));
    }
}
