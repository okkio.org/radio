package org.okkio.radio.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.squareup.otto.Subscribe;

import org.okkio.radio.R;
import org.okkio.radio.adapter.AutofitRecyclerView;
import org.okkio.radio.adapter.CustomItemDecoration;
import org.okkio.radio.adapter.GridItemAdapter;
import org.okkio.radio.event.RefreshUIEvent;
import org.okkio.radio.model.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryFragment extends BaseFragment {

    private List<Category> mCategoryList = new ArrayList<>();
    private GridItemAdapter mAdapter;
    private int mChoiceMode;
    private boolean mIsDualPane;

    public CategoryFragment() {
    }

    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    @Override
    public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.GridItemFragment, 0, 0);
        mChoiceMode = typedArray.getInt(R.styleable.GridItemFragment_android_choiceMode, AbsListView.CHOICE_MODE_NONE);
        boolean autoSelectView = typedArray.getBoolean(R.styleable.GridItemFragment_autoSelectView, false);
        typedArray.recycle();
    }

    public void setCategoryData(ArrayList<Category> list) {
        mCategoryList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        AutofitRecyclerView recyclerView = (AutofitRecyclerView) inflater.inflate(R.layout.grid_recycler, container, false);
        recyclerView.addItemDecoration(new CustomItemDecoration(getResources().getDimensionPixelSize(R.dimen.dimen_xsmall)));
        recyclerView.setHasFixedSize(true);

        mAdapter = new GridItemAdapter(mCategoryList, getActivity(), mChoiceMode);
        recyclerView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mAdapter.onRestoreInstanceState(savedInstanceState);
        }
        return recyclerView;
    }


    @Subscribe
    public void refreshUi(RefreshUIEvent event) {
        String refreshEvent = event.getRefreshEvent();
        if (refreshEvent.equals(RefreshUIEvent.REFRESH_CATEGORY_LIST_UI)) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            if (mIsDualPane) {
                mAdapter.setInitialView(0);
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        mAdapter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void isDualPane(boolean value) {
        mIsDualPane = value;
    }
}
