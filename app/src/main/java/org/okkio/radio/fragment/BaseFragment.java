package org.okkio.radio.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.squareup.otto.Bus;

import org.okkio.radio.App;
import org.okkio.radio.event.BaseEvent;

public class BaseFragment extends Fragment {
    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppBus().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getAppBus().unregister(this);
    }

    protected void postToBus(BaseEvent event) {
        App.postToBus(event);
    }

    protected Bus getAppBus() {
        return App.getInstance().getBus();
    }
}
