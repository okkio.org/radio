package org.okkio.radio.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.adapter.CustomItemDecoration;
import org.okkio.radio.adapter.ListItemAdapter;
import org.okkio.radio.data.StationDataCache;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.event.StationThreadCompletionEvent;
import org.okkio.radio.model.Station;
import org.okkio.radio.network.StationThread;
import org.okkio.radio.util.Helper;

import java.util.LinkedList;
import java.util.List;

public class StationFragment extends BaseFragment {

    private static final String BUNDLE_PAGE_NUMBER = "page_number";
    private List<Station> mStationList = new LinkedList<>();
    private ListItemAdapter mAdapter;
    private Long mCategoryId;
    private boolean mIsStarted = false;
    private int mPageCount = 0;
    private RecyclerView mRecyclerView;

    private int mPreviousTotal, mVisibleThreshold, mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private boolean mLoading = true;

    public StationFragment() {
    }

    public static StationFragment newInstance(Long categoryId/*, int icon*/) {
        StationFragment fragment = new StationFragment();
        Bundle args = new Bundle();
        args.putLong(App.KEY_CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCategoryId = getArguments().getLong(App.KEY_CATEGORY_ID);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_recycler, container, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new CustomItemDecoration(getResources().getDimensionPixelSize(R.dimen.dimen_space)));
        mAdapter = new ListItemAdapter(mStationList, getActivity());
        if (isAdded())
            mRecyclerView.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mPageCount = savedInstanceState.getInt(BUNDLE_PAGE_NUMBER);
            setStationList();
        } else {
            downloadStationData();
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                mVisibleItemCount = mRecyclerView.getChildCount();
                mTotalItemCount = layoutManager.getItemCount();
                mFirstVisibleItem = layoutManager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if (mTotalItemCount > mPreviousTotal) {
                        mLoading = false;
                        mPreviousTotal = mTotalItemCount;
                    }
                }
                if (!mLoading && (mTotalItemCount - mVisibleItemCount) <= (mFirstVisibleItem + mVisibleThreshold)) {
                    downloadStationData();
                    mLoading = true;
                }
            }

        });

        return view;
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BUNDLE_PAGE_NUMBER, mPageCount);
    }


    private void downloadStationData() {
        if (getActivity() != null && Helper.isClientConnected(getActivity())) {
            if (!mIsStarted) {
                mIsStarted = true;
                ++mPageCount;
                new StationThread("StationThread", getActivity(), mCategoryId, mPageCount).start();
            }
        } else {
            App.postToBus(new MessageEvent("Not connected, check connection"));
        }
    }


    @Subscribe
    public void refreshUi(StationThreadCompletionEvent event) {
        if (event.isThreadComplete()) {
            mIsStarted = false;
            mAdapter.clear();
            setStationList();
            if (mStationList.size() > 20)
                Helper.showSnackbar(mRecyclerView, "Found " + mStationList.size() + " stations so far");
        }
        if (event.isDownloadComplete()) {
            Helper.showSnackbar(mRecyclerView, "Found, " + mStationList.size() + " stations in total");
        }
    }

    private void setStationList() {
        List<Station> list = new LinkedList<>(StationDataCache.getStationDataCache().getStationList());
        mAdapter.addAll(list);
        mAdapter.notifyDataSetChanged();
    }
}
