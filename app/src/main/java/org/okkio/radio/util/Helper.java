package org.okkio.radio.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.okkio.radio.model.Station;
import org.okkio.radio.model.Stream;

import java.util.ArrayList;
import java.util.Random;

public class Helper {
    private Helper() {
        throw new AssertionError();
    }

    public static void hideKeyboard(Activity activity, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Check that a connection is available
     *
     * @param context
     * @return
     */
    public static boolean isClientConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }


    /**
     * Retrieve the stream url from the station object with a status of 1 or greater
     *
     * @param stn
     * @return
     */
    public static String getStream(Station stn) {
        String url = null;
        ArrayList<Stream> streams = (ArrayList<Stream>) stn.getStreams();
        int status;
        for (Stream stream : streams) {
            status = stream.getStatus();
            if (status >= 0) {
                url = stream.getStream();
                if (url != null && !url.isEmpty())
                    break;
            }
        }
        return url;
    }

    /**
     * Launch the transition on devices with api 21+, ignored on older devices
     *
     * @param activity
     * @param intent
     */
    public static void launchActivity(Activity activity, Intent intent) {
        @SuppressWarnings("unchecked")
        ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity);
        ActivityCompat.startActivity(activity, intent, activityOptions.toBundle());
    }

    /**
     * Fade the supplied view  element - either in or out
     *
     * @param view
     * @param visibility
     * @param opacityStart
     * @param opacityEnd
     */
    public static void fadeViewElement(final View view, final int visibility, int opacityStart, int opacityEnd) {
        Animation fadeOut = new AlphaAnimation(opacityStart, opacityEnd);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(300);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(visibility);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        view.startAnimation(fadeOut);
    }

    /**
     * Get material color with deep color for gradient
     *
     * @return
     */
    public static int[] getRandomGradient() {
        ArrayList<int[]> gradients = new ArrayList<>();
        gradients.add(new int[]{0xFFf44336, 0xFFd32f2f}); // red
        gradients.add(new int[]{0xFFc2185b, 0xFFe91e63}); // pink
        gradients.add(new int[]{0xFF7b1fa2, 0xFF9c27b0}); // purple
        gradients.add(new int[]{0xFF512da8, 0xFF673ab7}); // deep purple
        gradients.add(new int[]{0xFF303f9f, 0xFFc5cae9}); // indigo
        gradients.add(new int[]{0xFF1976d2, 0xFF2196f3}); // blue
        gradients.add(new int[]{0xFF0288d1, 0xFF03a9f4}); // light blue
        gradients.add(new int[]{0xFF0097a7, 0xFF00bcd4}); // cyan
        gradients.add(new int[]{0xFF00796b, 0xFF009688}); // teal
        gradients.add(new int[]{0xFF388e3c, 0xFF4caf50}); // green
        gradients.add(new int[]{0xFF689f38, 0xFF8bc34a}); // light green
        gradients.add(new int[]{0xFFafb42b, 0xFFcddc39}); // lime
        gradients.add(new int[]{0xFFfbc02d, 0xFFffeb3b}); // yellow
        gradients.add(new int[]{0xFFffa000, 0xFFffc107}); // amber
        gradients.add(new int[]{0xFFf57c00, 0xFFff9800}); // orange
        gradients.add(new int[]{0xFFe64a19, 0xFFff5722}); // deep orange
        gradients.add(new int[]{0xFF5d4037, 0xFF795548}); // brown
        gradients.add(new int[]{0xFF616161, 0xFF9e9e9e}); // grey
        gradients.add(new int[]{0xFF455a64, 0xFF607d8b}); // blue grey
        Random random = new Random();
        return gradients.get(random.nextInt(gradients.size()));
    }
}
