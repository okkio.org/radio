package org.okkio.radio.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.View;

import com.squareup.otto.Subscribe;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.data.StationDataCache;
import org.okkio.radio.event.DataModelUpdateEvent;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.event.OnClickEvent;
import org.okkio.radio.fragment.CategoryDataFragment;
import org.okkio.radio.fragment.CategoryFragment;
import org.okkio.radio.fragment.StationFragment;
import org.okkio.radio.model.Category;
import org.okkio.radio.util.Helper;

import java.util.List;

public class MainActivity extends BaseActivity {

    private CategoryDataFragment mCategoryDataFragment;
    private CategoryFragment mCategoryFragment;
    private StationFragment mStationFragment;
    private CoordinatorLayout mCoordinatorLayout;
    private Long mCategoryId;
    private boolean mDualPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCoordinatorLayout = findViewById(R.id.coordinator_layout);

        setToolbarOnActivity(R.id.toolbar);

        mCategoryDataFragment = (CategoryDataFragment) getSupportFragmentManager().findFragmentByTag(CategoryDataFragment.CATEGORY_DATA_FRAGMENT_TAG);
        if (mCategoryDataFragment == null) {
            mCategoryDataFragment = CategoryDataFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(mCategoryDataFragment, CategoryDataFragment.CATEGORY_DATA_FRAGMENT_TAG)
                    .commit();
        }

        mCategoryFragment = (CategoryFragment) getSupportFragmentManager().findFragmentById(R.id.category_grid_fragment);
        if (mCategoryDataFragment != null && mCategoryFragment != null) {
            mCategoryFragment.setCategoryData(mCategoryDataFragment.getCategoryData());
        }

        View stationList = findViewById(R.id.station_fragment_container);
        mDualPane = stationList != null && stationList.getVisibility() == View.VISIBLE;

        if (savedInstanceState != null) {
            mCategoryId = savedInstanceState.getLong(App.KEY_CATEGORY_ID);
        } else {
            if (mDualPane) {
                mCategoryId = 71l; // @todo: remove
            }
        }

        // instantiate the station UI on tablet device
        if (mDualPane) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for (int i = 0; i < fragments.size(); i++) {
                Fragment fragment = fragments.get(i);
                if (fragment instanceof CategoryFragment) {
                    CategoryFragment gridItemFragment = (CategoryFragment) fragment;
                    gridItemFragment.isDualPane(true);
                }
            }

            mStationFragment = (StationFragment) getSupportFragmentManager().findFragmentById(R.id.station_fragment_container);
            if (mStationFragment == null) {
                mStationFragment = StationFragment.newInstance(mCategoryId);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.station_fragment_container, mStationFragment)
                        .commit();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCategoryId != null) {
            outState.putLong(App.KEY_CATEGORY_ID, mCategoryId);
        }
    }

    // handle click events on both category and station items
    @Subscribe
    public void getOnClickEvent(OnClickEvent event) {

        if (event.getClickEvent().equals(OnClickEvent.GRID_ITEM_CLICK_EVENT)) {
            Category item = mCategoryDataFragment.getCategoryDataItem(event.getPosition());
            mCategoryId = item.getId();
            String categoryTitle = item.getTitle();
            StationDataCache.getStationDataCache().clearDataCache();

            if (mDualPane) {
                mStationFragment = StationFragment.newInstance(mCategoryId);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.station_fragment_container, mStationFragment)
                        .commit();
            } else {
                Intent intent = new Intent(this, StationActivity.class);
                intent.putExtra(App.KEY_CATEGORY_ID, mCategoryId);
                intent.putExtra(App.KEY_CATEGORY_TITLE, categoryTitle);
                Helper.launchActivity(MainActivity.this, intent);
            }

        }
        // handle clicks to station items
        else if (event.getClickEvent().equals(OnClickEvent.LIST_ITEM_CLICK_EVENT)) {
            int position = event.getPosition();
            Intent intent = new Intent(this, RadioPlayerActivity.class);
            intent.putExtra(App.KEY_QUEUE_POSITION, position);
            Helper.launchActivity(MainActivity.this, intent);
        }
    }

    // handle data model update events
    @Subscribe
    public void dataModelUpdate(DataModelUpdateEvent event) {
        String update = event.getDataModel();
        if (update.equals(DataModelUpdateEvent.CATEGORY_MODEL_DATA)) {
            if (mCategoryDataFragment != null && mCategoryFragment != null) {
                mCategoryFragment.setCategoryData(mCategoryDataFragment.getCategoryData());
            }
        }
    }

    // handle message events
    @Subscribe
    public void getMessageEvent(MessageEvent event) {
        Helper.showSnackbar(mCoordinatorLayout, event.getMessage());
    }
}
