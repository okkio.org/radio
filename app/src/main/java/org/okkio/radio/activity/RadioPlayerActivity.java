package org.okkio.radio.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.fragment.RadioPlayerFragment;

public class RadioPlayerActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_radio_player);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AppBarLayout appbar = findViewById(R.id.appbar);
            appbar.setElevation(0.0f);
        }

        // retrieve the queue position from the intent
        int position = getIntent().getIntExtra(App.KEY_QUEUE_POSITION, 0);

        if (getSupportFragmentManager().findFragmentById(R.id.radio_player_fragment_container) == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.radio_player_fragment_container, RadioPlayerFragment.newInstance(position))
                    .commit();
        }
    }


}
