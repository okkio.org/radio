package org.okkio.radio.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;

import com.squareup.otto.Subscribe;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.event.OnClickEvent;
import org.okkio.radio.fragment.StationFragment;
import org.okkio.radio.util.Helper;

public class StationActivity extends BaseActivity {

    private CoordinatorLayout mCoordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_station);
        mCoordinatorLayout = findViewById(R.id.coordinator_layout);

        Long id = getIntent().getLongExtra(App.KEY_CATEGORY_ID, 0);
        String title = getIntent().getStringExtra(App.KEY_CATEGORY_TITLE);

        setToolbarOnChildActivity(R.id.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title + " stations");
        }

        StationFragment stationFragment = (StationFragment) getSupportFragmentManager().findFragmentById(R.id.station_fragment_container);
        if (stationFragment == null) {
            stationFragment = StationFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.station_fragment_container, stationFragment)
                    .commit();
        }
    }

    @Subscribe
    public void getOnClickEvent(OnClickEvent event) {
        if (event.getClickEvent().equals(OnClickEvent.LIST_ITEM_CLICK_EVENT)) {
            int position = event.getPosition();
            Intent intent = new Intent(this, RadioPlayerActivity.class);
            intent.putExtra(App.KEY_QUEUE_POSITION, position);
            Helper.launchActivity(StationActivity.this, intent);
        }
    }

    // handle message events
    @Subscribe
    public void getMessageEvent(MessageEvent event) {
        Helper.showSnackbar(mCoordinatorLayout, event.getMessage());
    }

}
