package org.okkio.radio.activity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.squareup.otto.Bus;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.event.BaseEvent;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setToolbar(int toolbarId) {
        mToolbar = findViewById(toolbarId);
        setSupportActionBar(mToolbar);
    }

    protected void setToolbarOnActivity(int toolbarId) {
        setToolbar(toolbarId);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    protected void setToolbarOnChildActivity(int toolbar) {
        setToolbar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mToolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
            }
        }
    }

    // setup the event bus
    @Override
    protected void onResume() {
        super.onResume();
        getAppBus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getAppBus().unregister(this);
    }

    protected Bus getAppBus() {
        return App.getInstance().getBus();
    }

    protected void postToBus(BaseEvent event) {
        App.postToBus(event);
    }
}
