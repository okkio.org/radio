package org.okkio.radio.event;

import org.okkio.radio.model.Category;

import java.util.List;

public class CategoryThreadCompletionEvent extends BaseEvent {

    private List<Category> mCategoryList;

    public CategoryThreadCompletionEvent(List<Category> categoryList) {
        mCategoryList = categoryList;
    }

    public List<Category> getCategoryList() {
        return mCategoryList;
    }
}
