package org.okkio.radio.data;

import org.okkio.radio.model.Station;

import java.util.LinkedList;
import java.util.List;

/**
 * Singleton data cache used to hold the station list, accessible anywhere within the app
 */

public class StationDataCache {

    private static StationDataCache sDataCache;
    private List<Station> mList = new LinkedList<>();

    private StationDataCache() {
    }

    public static StationDataCache getStationDataCache() {
        if (sDataCache == null) {
            sDataCache = new StationDataCache();
        }
        return sDataCache;
    }

    public List<Station> getStationList() {
        return mList;
    }

    public void setStationList(LinkedList<Station> list) {
        mList.addAll(list);
    }

    public Station getStation(int position) {
        return mList.get(position);
    }

    public void clearDataCache() {
        if (mList.size() > 0) {
            mList.clear();
        }
    }
}
