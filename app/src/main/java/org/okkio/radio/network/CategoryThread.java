package org.okkio.radio.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.event.CategoryThreadCompletionEvent;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.model.Category;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryThread extends Thread {
    private static final String TAG = "CategoryThread";
    private static final String BASE_URL = "http://api.dirble.com/v2/categories/primary?token=";
    private Context mContext;

    public CategoryThread(String threadName, Context context) {
        super(threadName);
        mContext = context;
    }

    @Override
    public void run() {
        try {
            String token = mContext.getResources().getString(R.string.dirble_api_key);
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(BASE_URL + token).build();
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                Reader in = response.body().charStream();
                BufferedReader reader = new BufferedReader(in);
                Category[] array = new Gson().fromJson(reader, Category[].class);
                if (array != null) {
                    List<Category> categories = new ArrayList<>(Arrays.asList(array));
                    App.postToBus(new CategoryThreadCompletionEvent(categories));
                } else {
                    App.postToBus(new MessageEvent("No results received"));
                }
                reader.close();
            } else {
                Log.e(TAG, "Failed to fetch JSON" + response.toString());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception parsing json:" + e.getMessage());
        }
    }
}
