package org.okkio.radio.network;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.data.StationDataCache;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.event.StationThreadCompletionEvent;
import org.okkio.radio.model.Station;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.LinkedList;

public class StationThread extends Thread {
    private static final String TAG = "StationThread";
    private static final String BASE_URL = "http://api.dirble.com/v2/category/";
    private static final String QUERY = "/stations?";
    private static final String PAGE_PARAM = "page";
    private static final String RESULTS_PER_PAGE = "per_page";
    private static final String TOKEN_PARAM = "token";

    private Context mContext;
    private Long mCategoryId;
    private int mPage = 1;

    public StationThread(String threadName, Context context, Long categoryId, int pageNumber) {
        super(threadName);
        mContext = context;
        mCategoryId = categoryId;
        mPage = pageNumber;
    }

    @Override
    public void run() {
        int resultsPerPage = 20;

        String token = mContext.getResources().getString(R.string.dirble_api_key);
        Uri stationUri = Uri.parse(BASE_URL + mCategoryId + QUERY).buildUpon()
                .appendQueryParameter(PAGE_PARAM, String.valueOf(mPage))
                .appendQueryParameter(RESULTS_PER_PAGE, String.valueOf(resultsPerPage))
                .appendQueryParameter(TOKEN_PARAM, token)
                .build();

        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(stationUri.toString()).build();
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                Reader in = response.body().charStream();
                BufferedReader reader = new BufferedReader(in);

                Station[] data = new Gson().fromJson(reader, Station[].class);
                if (data != null) {

                    if (data.length == 0 && mPage > 1) {
                        App.postToBus(new StationThreadCompletionEvent(true, true));
                    } else {
                        StationDataCache.getStationDataCache().setStationList(new LinkedList<>(Arrays.asList(data)));
                        App.postToBus(new StationThreadCompletionEvent(true, false));
                    }

                } else {
                    App.postToBus(new MessageEvent("No results available"));
                }
                reader.close();
            }

        } catch (Exception e) {
            Log.e(TAG, "Failed to fetch JSON", e);
        }
    }
}
