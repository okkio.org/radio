package org.okkio.radio.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.app.NotificationCompat.MediaStyle;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.activity.RadioPlayerActivity;
import org.okkio.radio.data.StationDataCache;
import org.okkio.radio.event.MessageEvent;
import org.okkio.radio.event.PlaybackServiceEvent;
import org.okkio.radio.event.QueuePositionEvent;
import org.okkio.radio.model.Station;
import org.okkio.radio.util.Helper;

import java.io.IOException;
import java.util.List;

public class PlaybackService extends Service implements
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = "PlaybackService";
    private static final String CHANNEL_ID = "media_playback_channel";
    private static final int NOTIFY_ID = 101;

    public static final String EXTRA_STATION_URI = "station_uri";
    public static final String EXTRA_STATION_NAME = "station_name";
    public static final String EXTRA_STATION_SLUG = "station_slug";
    public static final String EXTRA_STATION_COUNTRY = "station_country";
    public static final String EXTRA_STATION_IMAGE_URL = "station_image_url";
    public static final String EXTRA_STATION_THUMB_URL = "station_thumb_url";
    public static final String EXTRA_STATION_QUEUE_POSITION = "queue_position";
    public static final String ACTION_PLAY = "play";
    public static final String ACTION_STOP = "updateSession";
    public static final String ACTION_NEXT = "next";
    public static final String ACTION_PREV = "prev";
    public static final String ACTION_OPEN = "open";

    private NotificationManager mNotificationManager;
    private WifiManager.WifiLock mWifiLock;
    private AudioManager mAudioManager;
    private MediaSessionCompat mMediaSession;
    private PlaybackStateCompat mPlaybackState;
    private MediaControllerCompat mMediaController;
    private MediaPlayer mMediaPlayer;
    private Binder mBinder = new ServiceBinder();
    private boolean mIsRegistered;
    private List<Station> mQueue;
    private int mQueuePosition;
    private MediaMetadataCompat mMetadata;

    private final IntentFilter mNoisyIntentFilter =
            new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);

    private final BroadcastReceiver mNoisyBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                // Headphones removed - stop playback
                mMediaController.getTransportControls().stop();
                App.postToBus(new PlaybackServiceEvent(PlaybackServiceEvent.ON_BECOMING_NOISY));
            }
        }
    };


    public PlaybackService() {
    }

    public class ServiceBinder extends Binder {

        public ServiceBinder() {
        }

        public PlaybackService getService() {
            return PlaybackService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public MediaSessionCompat.Token getMediaSessionToken() {
        return mMediaSession.getSessionToken();
    }

    @Override
    public int onStartCommand(Intent startIntent, int flags, int startId) {
        if (startIntent != null && startIntent.getAction() != null) {

            switch (startIntent.getAction()) {
                case ACTION_PLAY:
                    mMediaController.getTransportControls().play();
                    break;
                case ACTION_STOP:
                    mMediaController.getTransportControls().stop();
                    break;
                case ACTION_NEXT:
                    mMediaController.getTransportControls().skipToNext();
                    break;
                case ACTION_PREV:
                    mMediaController.getTransportControls().skipToPrevious();
                    break;
            }
        }
        MediaButtonReceiver.handleIntent(mMediaSession, startIntent);
        return super.onStartCommand(startIntent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mQueue = StationDataCache.getStationDataCache().getStationList();

        mPlaybackState = updatePlaybackState(PlaybackStateCompat.STATE_NONE);
        mMediaSession = new MediaSessionCompat(this, TAG);
        mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSession.setCallback(new MediaSessionCallback());
        mMediaSession.setPlaybackState(mPlaybackState);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        initMediaPlayer();

        try {
            mMediaController = new MediaControllerCompat(this, mMediaSession.getSessionToken());
        } catch (RemoteException e) {
            Log.e(TAG, "Error instantiating Media Controller: %s" + e.getMessage());
        }

        // create the wifi lock
        mWifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL, TAG);
        App.getInstance().getBus().register(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        App.getInstance().getBus().unregister(this);
        releaseResources();
    }


    @Override
    public void onAudioFocusChange(int focusChange) {
        if (focusChange == AudioManager.AUDIOFOCUS_LOSS ||
                focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {

            mMediaPlayer.stop();
            updateSession(PlaybackStateCompat.STATE_STOPPED, PlaybackServiceEvent.ON_AUDIO_FOCUS_LOSS);
            raiseNotification();
        }
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        // request audio focus
        int audioFocus = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        // register noisy broadcast receiver
        registerNoisy();

        // if we've gained focus, start playback
        if (audioFocus == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mMediaPlayer.start();
            mMediaSession.setActive(true);
            mPlaybackState = updatePlaybackState(PlaybackStateCompat.STATE_PLAYING);
            mMediaSession.setPlaybackState(mPlaybackState);
            raiseNotification();

            // post event, allowing the PlayerActivity to hide the progress bar
            App.postToBus(new PlaybackServiceEvent(PlaybackServiceEvent.ON_BUFFERING_COMPLETE));
        } else {
            App.postToBus(new PlaybackServiceEvent(PlaybackServiceEvent.ON_AUDIO_FOCUS_LOSS));
        }
    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.reset();
        updateSession(PlaybackStateCompat.STATE_NONE, PlaybackServiceEvent.ON_PLAYBACK_COMPLETION);
    }


    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mMediaPlayer.reset();
        updateSession(PlaybackStateCompat.STATE_NONE, PlaybackServiceEvent.ON_PLAYBACK_ERROR);
        return true;
    }


    private final class MediaSessionCallback extends MediaSessionCompat.Callback {
        @Override
        public void onPlayFromSearch(String query, Bundle extras) {
            Uri uri = extras.getParcelable(EXTRA_STATION_URI);
            mQueuePosition = extras.getInt(EXTRA_STATION_QUEUE_POSITION);
            onPlayFromUri(uri, extras);
        }

        @Override
        public void onPlayFromUri(Uri uri, Bundle extras) {

            try {
                int state = mPlaybackState.getState();
                if (state == PlaybackStateCompat.STATE_NONE || state == PlaybackStateCompat.STATE_STOPPED) {
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(PlaybackService.this, uri);
                    mMediaPlayer.prepareAsync(); // calls onPrepared() when complete
                    mPlaybackState = updatePlaybackState(PlaybackStateCompat.STATE_BUFFERING);
                    mMediaSession.setPlaybackState(mPlaybackState);
                    mMediaSession.setMetadata(new MediaMetadataCompat.Builder()
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, extras.getString(EXTRA_STATION_NAME))
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, extras.getString(EXTRA_STATION_SLUG))
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_DESCRIPTION, extras.getString(EXTRA_STATION_COUNTRY))
                            .putString(MediaMetadataCompat.METADATA_KEY_ART_URI, extras.getString(EXTRA_STATION_IMAGE_URL))
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON_URI, extras.getString(EXTRA_STATION_THUMB_URL))
                            .build());
                    raiseNotification();
                    // acquire wifi lock to prevent wifi going to sleep while playing
                    mWifiLock.acquire();
                }

            } catch (IOException e) {
                Log.e(TAG, "Error buffering audio stream");
            }

        }


        @Override
        public void onStop() {
            int state = mPlaybackState.getState();
            if (state == PlaybackStateCompat.STATE_PLAYING || state == PlaybackStateCompat.STATE_BUFFERING) {
                mMediaPlayer.stop();
                updateSession(PlaybackStateCompat.STATE_STOPPED, PlaybackServiceEvent.ON_STOP);
                raiseNotification();

            }
            if (state == PlaybackStateCompat.STATE_BUFFERING) {
                App.postToBus(new PlaybackServiceEvent(PlaybackServiceEvent.ON_BUFFERING_COMPLETE));
            }
        }


        @Override
        public void onSkipToNext() {
            super.onSkipToNext();
            ++mQueuePosition;
            checkQueuePosition();
        }


        @Override
        public void onSkipToPrevious() {
            super.onSkipToPrevious();
            --mQueuePosition;
            checkQueuePosition();
        }

    }

    private void checkQueuePosition() {
        if (mQueuePosition >= 0 && mQueuePosition < mQueue.size()) {
            playFromQueue();
            App.postToBus(new QueuePositionEvent(mQueuePosition));
        } else {
            App.postToBus(new MessageEvent("Index out of bounds"));
        }
    }


    private void playFromQueue() {
        Station stn = mQueue.get(mQueuePosition);
        String name = stn.getName() != null ? stn.getName() : "";
        String slug = stn.getSlug() != null ? stn.getSlug() : "";
        String country = stn.getCountry() != null ? stn.getCountry() : "";
        String imageUrl = stn.getImage().getUrl() != null ? stn.getImage().getUrl() : "";
        String thumbUrl = stn.getImage().getThumb().getUrl() != null ? stn.getImage().getThumb().getUrl() : "";

        String url = Helper.getStream(stn);
        if (url != null) {
            Uri uri = Uri.parse(url);
            try {
                int state = mPlaybackState.getState();
                if (state == PlaybackStateCompat.STATE_NONE || state == PlaybackStateCompat.STATE_STOPPED) {
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(PlaybackService.this, uri);
                    mMediaPlayer.prepareAsync(); // calls onPrepared() when complete
                    mPlaybackState = updatePlaybackState(PlaybackStateCompat.STATE_BUFFERING);
                    mMediaSession.setPlaybackState(mPlaybackState);
                    mMediaSession.setMetadata(new MediaMetadataCompat.Builder()
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, name)
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, slug)
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_DESCRIPTION, country)
                            .putString(MediaMetadataCompat.METADATA_KEY_ART_URI, imageUrl)
                            .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON_URI, thumbUrl)
                            .build());
                    raiseNotification();
                    mWifiLock.acquire();
                }

            } catch (IOException e) {
                Log.e(TAG, "Error buffering audio stream");
            }
        } else {
            App.postToBus(new PlaybackServiceEvent(PlaybackServiceEvent.ON_NO_STREAM_FOUND));
        }
    }


    private void initMediaPlayer() {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setLooping(false);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
    }

    private void updateSession(int playbackState, String event) {
        mAudioManager.abandonAudioFocus(this);
        mMediaSession.setActive(false);
        unregisterNoisy();

        mPlaybackState = updatePlaybackState(playbackState);
        mMediaSession.setPlaybackState(mPlaybackState);
        App.postToBus(new PlaybackServiceEvent(event));
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void releaseResources() {
        releaseMediaPlayer();
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
            mWifiLock = null;
        }

        if (mMediaSession != null) {
            mMediaSession.release();
            mMediaSession = null;
        }
        mNotificationManager.cancel(NOTIFY_ID);
    }

    private PlaybackStateCompat updatePlaybackState(int playbackState) {
        return new PlaybackStateCompat.Builder()
                .setState(playbackState, 0, 1.0f)
                .build();
    }

    private void registerNoisy() {
        if (!mIsRegistered) {
            registerReceiver(mNoisyBroadcastReceiver, mNoisyIntentFilter);
            mIsRegistered = true;
        }
    }

    private void unregisterNoisy() {
        if (mIsRegistered) {
            unregisterReceiver(mNoisyBroadcastReceiver);
            mIsRegistered = false;
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        CharSequence name = "Media playback";
        String description = "Media playback controls";
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        mNotificationManager.createNotificationChannel(mChannel);
    }

    private void raiseNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
        mMetadata = mMediaController.getMetadata();
        MediaDescriptionCompat description = mMetadata.getDescription();
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo);
        Intent launchIntent = new Intent(getApplicationContext(), RadioPlayerActivity.class);
        launchIntent.setAction(ACTION_OPEN);
        PendingIntent returnToPlayer = PendingIntent.getActivity(this, 0, launchIntent, 0);

        Intent stopIntent = new Intent(getApplicationContext(), PlaybackService.class);
        stopIntent.setAction(ACTION_STOP);
        PendingIntent removeNotification = PendingIntent.getService(getApplicationContext(), 1, stopIntent, 0);


        // https://medium.com/androiddevelopers/migrating-mediastyle-notifications-to-support-android-o-29c7edeca9b7
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID);
        notificationBuilder
                .setStyle(
                        new MediaStyle()
                                .setShowCancelButton(true)
                                .setCancelButtonIntent(
                                        MediaButtonReceiver.buildMediaButtonPendingIntent(
                                                this, PlaybackStateCompat.ACTION_STOP)))
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setSmallIcon(R.drawable.ic_logo)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setOnlyAlertOnce(true)
                .setContentIntent(returnToPlayer)
                .setContentTitle(description.getTitle())
                .setContentText(description.getDescription())
                .setLargeIcon(bm)
                .setDeleteIntent(removeNotification);

        notificationBuilder.addAction(generateAction(R.drawable.ic_skip_previous_white_24dp, "Previous", ACTION_PREV));
        int state = mPlaybackState.getState();
        if (state == PlaybackStateCompat.STATE_PLAYING)
            notificationBuilder.addAction(generateAction(R.drawable.ic_stop_white_24dp, "Stop", ACTION_STOP));
        else
            notificationBuilder.addAction(generateAction(R.drawable.ic_play_arrow_white_24dp, "Play", ACTION_PLAY));
        notificationBuilder.addAction(generateAction(R.drawable.ic_skip_next_black_24dp, "Next", ACTION_NEXT));

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFY_ID, notificationBuilder.build());
    }

    private android.support.v4.app.NotificationCompat.Action generateAction(int icon, String title, String intentAction) {
        Intent intent = new Intent(getApplicationContext(), PlaybackService.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new android.support.v4.app.NotificationCompat.Action.Builder(icon, title, pendingIntent).build();
    }
}
