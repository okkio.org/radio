package org.okkio.radio.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Station implements Parcelable {
    private Long id;
    private String name;
    private String country;
    private Image image;
    private String slug;
    private String website;
    private String twitter;
    private String facebook;
    private int totalListeners = 0;
    private List<Stream> streams;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public Image getImage() {
        return image;
    }

    public String getSlug() {
        return slug;
    }

    public String getWebsite() {
        return website;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public int getTotalListeners() {
        return totalListeners;
    }

    public List<Stream> getStreams() {
        return streams;
    }

    @Override
    public String toString() {
        return String.format("%s", getName());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.country);
        dest.writeParcelable(this.image, 0);
        dest.writeString(this.slug);
        dest.writeString(this.website);
        dest.writeString(this.twitter);
        dest.writeString(this.facebook);
        dest.writeInt(this.totalListeners);
        dest.writeTypedList(streams);
    }

    public Station() {
    }

    protected Station(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.country = in.readString();
        this.image = in.readParcelable(Image.class.getClassLoader());
        this.slug = in.readString();
        this.website = in.readString();
        this.twitter = in.readString();
        this.facebook = in.readString();
        this.totalListeners = in.readInt();
        this.streams = in.createTypedArrayList(Stream.CREATOR);
    }

    public static final Creator<Station> CREATOR = new Creator<Station>() {
        public Station createFromParcel(Parcel source) {
            return new Station(source);
        }

        public Station[] newArray(int size) {
            return new Station[size];
        }
    };
}
