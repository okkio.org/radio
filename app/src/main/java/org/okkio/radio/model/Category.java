package org.okkio.radio.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Category {
    private Long id;
    private String title;
    private String description;
    private String slug;
    private String ancestry;
    @JsonIgnore
    private Integer icon;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSlug() {
        return slug;
    }

    public String getAncestry() {
        return ancestry;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }
}
