package org.okkio.radio.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.event.OnClickEvent;
import org.okkio.radio.model.Category;
import org.okkio.radio.util.Helper;

public class GridItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mItemTitle;
    private ItemChoiceManager mItemChoiceManager;
    private int mPosition;

    public GridItemViewHolder(View itemView, ItemChoiceManager choiceManager) {
        super(itemView);
        itemView.setOnClickListener(this);
        mItemChoiceManager = choiceManager;

        mItemTitle = itemView.findViewById(R.id.item_title);
        View wrapper = itemView.findViewById(R.id.wrapper);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TL_BR, Helper.getRandomGradient());
        gd.setCornerRadius(5f);
        wrapper.setBackground(gd);
    }

    public void bindModelItem(Category item, Context context, int position) {
        mPosition = position;
        mItemTitle.setText(item.getTitle());
    }

    @Override
    public void onClick(View v) {
        App.postToBus(new OnClickEvent(OnClickEvent.GRID_ITEM_CLICK_EVENT, mPosition));
        mItemChoiceManager.onClick(this);
    }
}
