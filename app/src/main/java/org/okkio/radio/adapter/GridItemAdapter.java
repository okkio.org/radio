package org.okkio.radio.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.okkio.radio.R;
import org.okkio.radio.model.Category;

import java.util.List;

public class GridItemAdapter extends RecyclerView.Adapter<GridItemViewHolder> {

    private List<Category> mList;
    private Context mContext;
    private ItemChoiceManager mItemChoiceManager;

    public GridItemAdapter(List<Category> list, Context context, int choiceMode) {
        mList = list;
        mContext = context;
        mItemChoiceManager = new ItemChoiceManager(this);
        mItemChoiceManager.setChoiceMode(choiceMode);
    }

    @NonNull
    @Override
    public GridItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.grid_item, parent, false);
        return new GridItemViewHolder(view, mItemChoiceManager);
    }

    @Override
    public void onBindViewHolder(@NonNull GridItemViewHolder holder, int position) {
        Category item = mList.get(position);
        holder.bindModelItem(item, mContext, position);
        mItemChoiceManager.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        mItemChoiceManager.onRestoreInstanceState(savedInstanceState);
    }

    public void onSaveInstanceState(Bundle outState) {
        mItemChoiceManager.onSaveInstanceState(outState);
    }

    public int getSelectedItemPosition() {
        return mItemChoiceManager.getSelectedItemPosition();
    }

    public void selectView(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof GridItemViewHolder) {
            GridItemViewHolder gvh = (GridItemViewHolder) viewHolder;
            gvh.onClick(gvh.itemView);
        }
    }

    public void setInitialView(int position) {
        mItemChoiceManager.setInitialCheckedState(position);
    }
}
