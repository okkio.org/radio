package org.okkio.radio.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.okkio.radio.R;
import org.okkio.radio.model.Station;

import java.util.List;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemViewHolder> {

    private List<Station> mList;
    private Context mContext;
    private int mIcon;

    public ListItemAdapter(List<Station> list, Context context) {
        mList = list;
        mContext = context;
        mIcon = R.mipmap.ic_launcher_round;
    }

    @NonNull
    @Override
    public ListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder holder, int position) {
        Station item = mList.get(position);
        holder.bindStationItem(item, mContext, position, mIcon);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Station> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }


}
