package org.okkio.radio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.okkio.radio.App;
import org.okkio.radio.R;
import org.okkio.radio.event.OnClickEvent;
import org.okkio.radio.model.Station;

public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mItemTitle;
    private TextView mItemCountry;
    private TextView mItemListeners;
    private ImageView mItemIcon;
    private int mPosition;

    public ListItemViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        mItemTitle = itemView.findViewById(R.id.item_title);
        mItemListeners = itemView.findViewById(R.id.item_listeners);
        mItemCountry = itemView.findViewById(R.id.item_country);
        mItemIcon = itemView.findViewById(R.id.item_icon);
    }

    public void bindStationItem(Station item, Context context, int position, int icon) {
        mPosition = position;
        mItemTitle.setText(item.getName());
        mItemCountry.setText(item.getCountry());
        mItemListeners.setText(context.getString(R.string.total_listeners, item.getTotalListeners()));
        String url = item.getImage().getThumb().getUrl();
        if (url == null || url.isEmpty()) {
            url = item.getImage().getUrl();
        }

        Picasso.get()
                .load(url)
                .resize(60, 60)
                .placeholder(icon)
                .error(icon)
                .into(mItemIcon);
    }

    @Override
    public void onClick(View v) {
        App.postToBus(new OnClickEvent(OnClickEvent.LIST_ITEM_CLICK_EVENT, mPosition));
    }
}
